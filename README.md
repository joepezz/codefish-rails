Codefish
======================
![Codeship Status](https://lighthouse.codeship.io/bitbucket/f515f6f0-9c62-0132-655c-263ab955f60c/status)

This application is used for screencast tutorials about how to set up Codeship projects with Ruby on Rails applications.

Open a browser windows and copy and paste the following "codefish-rails-tutorial.herokuapp.com" on the URL field to run the Ruby on Rails app.

After you make changes to a local files do the following to make sure that such changes are pushed to the BitBucket repository:

For every file you edit locally make sure to "commit" the changes via git to publish them to the repository upon a git push command. See below:

git status

git commit -am "type comment here that explains the edits made"

git push origin master

git status

git status tells you what files have been edited.  
git commit commits what changes have been made
git push pushes committed files on local machine to the corresponding BitBucket repository.

If the above series of commands does not work try the following commands below:

git remote add origin <copy and paste the bitbucket URL for the repository here> 

git add .

git push